# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Virtual my fractal projects dependencies (for development)"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc qt5"
REQUIRED_USE=""

RDEPEND=">=media-libs/glm-0.9.9:*
	>=media-libs/glbinding-3.0:*
	>media-libs/globjects-1.1:*
	qt5? ( >=dev-qt/qtcore-5.9:5
		>=dev-qt/qtgui-5.9:5
		>=dev-qt/qtwidgets-5.9:5
		>=dev-qt/qtopengl-5.9:5
		>=media-libs/gloperate-2.0:*[qt5] )"
DEPEND="${RDEPEND}
	>=dev-util/cmake-3.9:*
	doc? ( >=app-doc/doxygen-1.8:* )"
