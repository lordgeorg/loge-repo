# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 cmake

DESCRIPTION="API specification and reference implementations for glyph rendering in 2D and 3D"
HOMEPAGE="https://github.com/cginternals/openll-cpp"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc examples static-libs tests"
REQUIRED_USE=""

RDEPEND=">media-libs/glm-0.9.8:*
	>=dev-cpp/cppassist-1.0:*
	>=dev-cpp/cppfs-1.0:*
	>=dev-cpp/cpplocate-1.0:*
	examples? ( >=media-libs/glfw-3.2:* )
	>=media-libs/glbinding-3.0:*
	>=media-libs/globjects-2.0:*"
DEPEND="${RDEPEND}
	>=dev-util/cmake-3.0
	doc? ( >=app-doc/doxygen-1.8:*[dot] )"

EGIT_REPO_URI="https://github.com/cginternals/openll-cpp.git"
EGIT_BRANCH="master"
#EGIT_COMMIT="HEAD"
EGIT_SUBMODULES=( '*' )

CMAKE_MAKEFILE_GENERATOR="emake"

PATCHES=("${FILESDIR}/0_version-${PVR}.patch"
	"${FILESDIR}/1_lib-${ARCH}.patch"
	"${FILESDIR}/2_docs-path.patch"
	"${FILESDIR}/3_glm.patch"
# remove when possible
	"${FILESDIR}/4_deprecated-copy.patch"
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DOPTION_BUILD_DOCS=$(usex doc)
		-DOPTION_BUILD_EXAMPLES=$(usex examples)
		-DOPTION_BUILD_TESTS=$(usex tests)
		-DBUILD_SHARED_LIBS=$(usex static-libs OFF ON)

#deactivating optional find_package calls
		-DCMAKE_DISABLE_FIND_PACKAGE_cppcheck=TRUE
		-DCMAKE_DISABLE_FIND_PACKAGE_clang_tidy=TRUE

		$(usex tests -DCMAKE_DISABLE_FIND_PACKAGE_PythonInterp=TRUE)
		$(usex tests -DCMAKE_DISABLE_FIND_PACKAGE_Threads=TRUE)
	)

	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_test() {
	cmake_src_test
}

src_install() {
	cmake_src_install
}
