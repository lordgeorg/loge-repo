# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 cmake

DESCRIPTION="C++ library strictly wrapping OpenGL objects"
HOMEPAGE="https://github.com/cginternals/globjects"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc examples +exceptions glfw qt5 static-libs tests"
REQUIRED_USE="glfw? ( examples )
	qt5? ( examples )"

RDEPEND="<media-libs/glbinding-3.1.0:*
	>media-libs/glm-0.9:*
	examples? ( dev-cpp/cpplocate:* )
	glfw? ( >=media-libs/glfw-3.2:* )
	qt5? ( >=dev-qt/qtcore-5.1:5
		>=dev-qt/qtgui-5.1:5
		>=dev-qt/qtwidgets-5.1:5 )"
DEPEND="${RDEPEND}
	>=dev-util/cmake-3.0:*
	doc? ( >=app-doc/doxygen-1.8:*[dot] )"

EGIT_REPO_URI="https://github.com/cginternals/globjects.git"
EGIT_BRANCH="master"
EGIT_COMMIT="v1.1.0"
EGIT_SUBMODULES=( '*' )

CMAKE_MAKEFILE_GENERATOR="emake"

PATCHES=("${FILESDIR}/1_lib-${ARCH}.patch"
	"${FILESDIR}/2_docs-path.patch"
	"${FILESDIR}/4_glbindings3.patch"
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DOPTION_BUILD_EXAMPLES=$(usex examples)
		-DOPTION_BUILD_DOCS=$(usex doc)
		-DOPTION_BUILD_TESTS=$(usex tests)
		-DOPTION_ERRORS_AS_EXCEPTION=$(usex exceptions)
		-DBUILD_SHARED_LIBS=$(usex static-libs OFF ON)

#deactivating optional find_package calls
		$(usex examples $(usex glfw "" -DCMAKE_DISABLE_FIND_PACKAGE_GLFW=TRUE))
		$(usex examples $(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Core=TRUE))
		$(usex examples $(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Gui=TRUE))
		$(usex examples $(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Widgets=TRUE))
	)

	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_test() {
	cmake_src_test
}

src_install() {
	cmake_src_install
}
