# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 cmake

DESCRIPTION="C++ library for defining and controlling GPU rendering/processing operations"
HOMEPAGE="https://github.com/cginternals/gloperate"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="assimp doc examples ffmpeg glfw hidapi qt5 qtquick static-libs tests"
REQUIRED_USE="assimp? ( examples )
	qtquick? ( qt5 )"

RDEPEND="
	>=dev-cpp/cppexpose-1.0:*
	>=dev-cpp/cppassist-1.0:*
	>=dev-cpp/cpplocate-1.0:*
	>=media-libs/glm-0.9.9:*
	>=media-libs/glkernel-0.2:*
	>=media-libs/glbinding-3.0.0:*
	>media-libs/globjects-1.1.0:*
	>=media-libs/openll-0.9:*
	glfw? ( >=media-libs/glfw-3.1:* )
	qt5? ( >=dev-qt/qtcore-5.1:5
		>=dev-qt/qtgui-5.1:5
		>=dev-qt/qtwidgets-5.1:5
		>=dev-qt/qtopengl-5.1:5 )
	qtquick? ( >=dev-qt/qtdeclarative-5.1:5
		>=media-libs/qmltoolbox-1.1.0:* )
	ffmpeg? ( media-video/ffmpeg:* )
	hidapi? ( dev-libs/hidapi:* )
	assimp? ( media-libs/assimp:* )"
DEPEND="${RDEPEND}
	>=dev-util/cmake-3.0:*
	doc? ( >=app-doc/doxygen-1.8:*[dot] )"

EGIT_REPO_URI="https://github.com/cginternals/gloperate.git"
EGIT_BRANCH="master"
EGIT_COMMIT="13a6363fdac1f9ff06d91c9bc99649eacd87078e"
EGIT_SUBMODULES=( '*' )

CMAKE_MAKEFILE_GENERATOR="emake"

PATCHES=("${FILESDIR}/0_version-${PVR}.patch"
	"${FILESDIR}/1_lib-${ARCH}.patch"
	"${FILESDIR}/2_docs-path.patch"
	"${FILESDIR}/3_findglm.patch"
	"${FILESDIR}/4_libav-deprecated.patch"
	"${FILESDIR}/5_qtquick-use.patch"
	"${FILESDIR}/6_assimp-use.patch"
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DOPTION_BUILD_EXAMPLES=$(usex examples)
		-DOPTION_BUILD_DOCS=$(usex doc)
		-DOPTION_BUILD_TESTS=$(usex tests)
		-DBUILD_SHARED_LIBS=$(usex static-libs OFF ON)

#deactivating optional find_package calls
		-DCMAKE_DISABLE_FIND_PACKAGE_cppcheck=TRUE
		-DCMAKE_DISABLE_FIND_PACKAGE_clang_tidy=TRUE

		$(usex ffmpeg "" -DCMAKE_DISABLE_FIND_PACKAGE_FFMPEG=TRUE)
		$(usex glfw "" -DCMAKE_DISABLE_FIND_PACKAGE_GLFW=TRUE)
		$(usex hidapi "" -DCMAKE_DISABLE_FIND_PACKAGE_HIDAPI=TRUE)

		$(usex examples $(usex assimp "" -DCMAKE_DISABLE_FIND_PACKAGE_ASSIMP=TRUE))

		$(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Core=TRUE)
		$(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Gui=TRUE)
		$(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Widgets=TRUE)
		$(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5OpenGL=TRUE)
		$(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Quick=TRUE)

		$(usex qtquick "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Quick=TRUE)
		$(usex qtquick "" -DCMAKE_DISABLE_FIND_PACKAGE_qmltoolbox=TRUE)
	)

	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_test() {
	cmake_src_test
}

src_install() {
	cmake_src_install
}
