# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 cmake

DESCRIPTION="Template for reliable, cross-platform C++ project setup using cmake"
HOMEPAGE="https://github.com/cginternals/cmake-init.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
#TODO coverage?
IUSE="doc examples qt5 static-libs tests"
REQUIRED_USE="qt5? ( examples )"

RDEPEND="examples? ( dev-cpp/cppassist:* )
		qt5? ( >=dev-qt/qtcore-5.1:5
		>=dev-qt/qtgui-5.1:5
		>=dev-qt/qtwidgets-5.1:5 )"
DEPEND="${RDEPEND}
	>=dev-util/cmake-3.0.0:*
	doc? ( >=app-doc/doxygen-1.8:*[dot] )"

EGIT_REPO_URI="https://github.com/cginternals/cmake-init.git"
EGIT_BRANCH="master"
#EGIT_COMMIT="HEAD"
EGIT_SUBMODULES=( '*' )

CMAKE_MAKEFILE_GENERATOR="emake"

PATCHES=("${FILESDIR}/0_version-${PV}.patch"
	"${FILESDIR}/1_lib-${ARCH}.patch"
	"${FILESDIR}/2_docs-path.patch"
	"${FILESDIR}/3_no-gtest-install.patch"
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DOPTION_BUILD_EXAMPLES=$(usex examples)
		-DOPTION_BUILD_DOCS=$(usex doc)
		-DBUILD_SHARED_LIBS=$(usex static-libs OFF ON)
#       -DOPTION_ENABLE_COVERAGE=...

#deactivating optional find_package calls
		-DCMAKE_DISABLE_FIND_PACKAGE_cppcheck=TRUE
		-DCMAKE_DISABLE_FIND_PACKAGE_clang_tidy=TRUE
		$(usex examples $(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Core=TRUE))
		$(usex examples $(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Gui=TRUE))
		$(usex examples $(usex qt5 "" -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Widgets=TRUE))
	)

	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_test() {
	cmake_src_test
}

src_install() {
	cmake_src_install
}
